// Frameworks
const Telegraf = require('telegraf')
const Telegram = require('telegraf/telegram')
const Koa = require('koa')
const koaBody = require('koa-body')
const debug = require('debug')('monitor:app.js')
const bodyParser = require('koa-bodyparser')
const moment = require('moment')

// Config
const config = require('./config.json')

const bot = new Telegraf(config.telegram.token)
bot.telegram.setWebhook(`${config.telegram.domain}${config.telegram.webtoken}`)
const telegram = new Telegram(config.telegram.token, {
  agent: null,
  webhookReply: true  
})

const app = new Koa()
app.use(koaBody())
app.use(bodyParser())
app.use(async (ctx, next) => {
  if (ctx.method !== 'POST' || ctx.url !== `/${config.telegram.webtoken}`) return next()
  await bot.handleUpdate(ctx.request.body, ctx.response)
  ctx.status = 200
})

app.use(async (ctx, next) => {
  if (ctx.method !== 'POST' || ctx.url !== `/` || ctx.header.signature !== config.silentdown.signature) return next()
  debug(ctx.request.body)
  let body = ctx.request.body
  moment.locale('zh-cn')
  if (body.down_at) body.down_at = moment(body.down_at, 'DD-MM-YYYY HH:mm')
  if (body.checked_at) body.checked_at = moment(body.checked_at, 'DD-MM-YYYY HH:mm')
  if (body.monitor_state === 'down') bot.telegram.sendMessage(config.telegram.targetuser, `*⚠️ 有网站炸了*\n*网站* ${body.location}\n*爆炸时间* ${body.down_at.fromNow()}\n*状态码* ${body.response_code}\n*最后检查时间* ${body.checked_at.fromNow()}`, { parse_mode: 'Markdown', disable_web_page_preview: true })
  else if (body.monitor_state === 'up') bot.telegram.sendMessage(config.telegram.targetuser, `*炸掉的网站回来了*\n*网站* ${body.location}\n*爆炸时间* ${body.down_at.fromNow()}\n*状态码* ${body.response_code}\n*最后检查时间* ${body.checked_at.fromNow()}`, { parse_mode: 'Markdown', disable_web_page_preview: true })
  else bot.telegram.sendMessage(config.telegram.targetuser, `监控 Webhook 收到一个事件，但返回的是一个不明格式。\n\`\`\`\n${JSON.stringify(body)}\n\`\`\``, { parse_mode: 'Markdown' })
  ctx.status = 204
})

app.listen(3001)
